package com.example.pc.flatmaps;

import android.app.Activity;
import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.example.pc.flatmaps.Dagger.DaggerAppComponent;
import com.example.pc.flatmaps.Dagger.MyModule;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;
import io.fabric.sdk.android.Fabric;

public class MyApp extends Application implements HasActivityInjector
{
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public void onCreate()
    {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

        DaggerAppComponent
                .builder()
                .myModule(new MyModule(this))
                .build()
                .inject(this);
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector()
    {
        return dispatchingAndroidInjector;
    }
}

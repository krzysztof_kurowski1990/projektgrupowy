package com.example.pc.flatmaps.Models.Requests;

import java.util.Date;

public class AddAdsRequest
{
    private String city;
    private String street;
    private String number;
    private String price;
    private String numberOfRooms;
    private String size;
    private String login;
    private Date date;

    public AddAdsRequest(String city, String street, String number, String price, String numberOfRooms, String size, String login, Date date) {
        this.city = city;
        this.street = street;
        this.number = number;
        this.price = price;
        this.numberOfRooms = numberOfRooms;
        this.size = size;
        this.login = login;
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(String numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}

package com.example.pc.flatmaps.Presenters;

import android.graphics.Bitmap;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.EditAdsFragment.EditAdsFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.EditAdsFragment.EditAdsFragmentView;
import com.example.pc.flatmaps.Models.Requests.EditAdsRequest;
import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;
import com.example.pc.flatmaps.Validator.InputValidator;
import com.google.gson.Gson;

import java.io.File;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.example.pc.flatmaps.Presenters.AddAdsFragmentPresenterImpl.MEDIA_TYPE_MULTIPART;
import static com.example.pc.flatmaps.Presenters.AddAdsFragmentPresenterImpl.MEDIA_TYPE_TEXT_PLAIN;
import static com.example.pc.flatmaps.Views.AddAdsFragment.PHOTO_PART_NAME;

public class EditAdsFragmentPresenterImpl implements EditAdsFragmentPresenter
{
    private EditAdsFragmentView mFragmentView;
    private UserInterface mUserInterface;
    private Subscriber<Response<Void>> mEditAdsSubscriber;
    private Observable<Response<Void>> mEditAdsObservable;

    @Inject
    public EditAdsFragmentPresenterImpl(EditAdsFragmentView editAdsFragmentView, UserInterface userInterface)
    {
        mFragmentView = editAdsFragmentView;
        mUserInterface = userInterface;
    }

    @Override
    public void onStart() {}

    @Override
    public void onPause(){}

    @Override
    public void onResume(){}

    @Override
    public void onDestroy()
    {
        Manager.unSubscribe(mEditAdsSubscriber);
    }

    @Override
    public void createBase64FromBitmap(Bitmap bitmap)
    {
        String imageBase64 = Manager.convertBitmapToBase64(bitmap);

        mFragmentView.base64Created(imageBase64);
    }

    @Override
    public void editAds(File imageFile, EditAdsRequest request)
    {
        if(!isImageCreated(imageFile))
        {
            mFragmentView.showToast("Please add photo first");
            return;
        }

        if(!isRequestValid(request))
        {
            mFragmentView.showToast(InputValidator.errorMessage);
            return;
        }

        createObservable(getImageBody(imageFile), request);

        createSubsciber();

        addAddsSubscribe();
        }

    private boolean isRequestValid(EditAdsRequest request)
    {
        return (InputValidator.isInputCorrect(
                request.getCity(),
                request.getLogin(),
                request.getNumber(),
                request.getNumberOfRooms(),
                request.getPrice(),
                request.getSize(),
                request.getStreet(),
                request.getAdsId()+"",
                request.getDateToday()+""));
    }

    private RequestBody getJsonBody(EditAdsRequest request)
    {
        String json = new Gson().toJson(request);
        return RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), json);
    }

    private MultipartBody.Part getImageBody(File imageFile)
    {
        RequestBody requestBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_MULTIPART), imageFile);
        return MultipartBody.Part.createFormData(PHOTO_PART_NAME, imageFile.getName(), requestBody);
    }

    private boolean isImageCreated(File imageFile)
    {
        return (imageFile != null);
    }

    private void createSubsciber()
    {
        mEditAdsSubscriber = new Subscriber<Response<Void>>() {
            @Override
            public void onCompleted()
            {
                mFragmentView.toggleSwipeLayout(false);
            }

            @Override
            public void onError(Throwable e)
            {
                mFragmentView.showToast(e.getMessage());

                mFragmentView.toggleSwipeLayout(false);
            }

            @Override
            public void onNext(Response<Void> response)
            {
                if(Manager.isResponseSuccessfull(response))
                    mFragmentView.onEditSuccessfull();
            }
        };
    }

    private void createObservable(MultipartBody.Part multipartBody, EditAdsRequest request)
    {
        mEditAdsObservable = mUserInterface.editAds(multipartBody, request);
    }

    private void addAddsSubscribe()
    {

        mFragmentView.toggleSwipeLayout(true);

        mEditAdsObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(mEditAdsSubscriber);
    }
}

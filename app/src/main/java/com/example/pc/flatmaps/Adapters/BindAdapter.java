package com.example.pc.flatmaps.Adapters;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.pc.flatmaps.R;
import com.squareup.picasso.Picasso;

public class BindAdapter {

    private static final int MAX_WIDTH = 1024;
    private static final int MAX_HEIGHT = 768;

    @BindingAdapter("android:src")
    public static void loadImage(ImageView view, String url)
    {
        int size = (int) Math.ceil(Math.sqrt(MAX_WIDTH * MAX_HEIGHT));

        Picasso
                .get()
                .load(url)
                .resize(size, size)
                .placeholder(R.drawable.ic_launcher_background)
                .error(R.drawable.ic_launcher_foreground)
                .into(view);
    }

    private static Bitmap getBitmapFromBase64(String base64)
    {
        byte[] decodedString = Base64.decode(base64, Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
    }

    @BindingAdapter("android:text")
    public static void loadText(TextView view, int text)
    {
        view.setText(String.valueOf(text));
    }

    @BindingAdapter("android:text")
    public static void loadText(TextView view, String text)
    {
        if(text != null && !text.isEmpty())
            view.setText(String.valueOf(text));
    }
}

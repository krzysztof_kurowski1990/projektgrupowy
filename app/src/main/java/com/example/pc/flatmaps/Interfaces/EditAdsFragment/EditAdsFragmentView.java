package com.example.pc.flatmaps.Interfaces.EditAdsFragment;

public interface EditAdsFragmentView
{
    public void showToast(String message);
    public void base64Created(String imageBase64);
    public void onEditSuccessfull();
    public void toggleSwipeLayout(boolean enabled);
}

package com.example.pc.flatmaps.Views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.DetailFragment.DetailFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.DetailFragment.DetailFragmentView;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.databinding.FragmentDetailBinding;
import com.google.gson.Gson;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

import static com.example.pc.flatmaps.Views.MapsFragment.ADS;

public class DetailFragment extends Fragment implements DetailFragmentView
{
    @Inject
    DetailFragmentPresenter mPresenter;

    @Inject
    Context mContext;

    private FragmentDetailBinding binder;
    private Bundle bundle;

    @Inject
    public DetailFragment(){}

    private AdsResponse getChoosenAds(){
        bundle = this.getArguments();
        String adsString = bundle.getString(ADS);

        return new Gson().fromJson(adsString, AdsResponse.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        int title = R.string.detail_fragment_title;
        Manager.changeActionBarTitle(getActivity(), title);

        AndroidSupportInjection.inject(this);

        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_detail, parent, false);
        binder.setFragment(this);
        binder.setAds(getChoosenAds());

        return binder.getRoot();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        binder.unbind();

        mPresenter.onDestroy();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.onStart();
    }

    @Override
    public void showToast(String message)
    {
        Manager.showToast(mContext, message);
    }

    public void onViewCreated(View view, Bundle savedInstanceState){}
}

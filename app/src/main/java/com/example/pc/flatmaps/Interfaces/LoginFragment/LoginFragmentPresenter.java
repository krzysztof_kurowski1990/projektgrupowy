package com.example.pc.flatmaps.Interfaces.LoginFragment;

import com.example.pc.flatmaps.Models.Requests.LoginRequest;

public interface LoginFragmentPresenter
{
    public void onStart();

    public void onDestroy();

    public void postLogin(LoginRequest loginRequest);
}

package com.example.pc.flatmaps.Views.Activities;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.DrawerInterface;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.Views.LoginFragment;
import com.example.pc.flatmaps.Views.MapsFragment;
import com.example.pc.flatmaps.Views.MyAdsFragment;
import com.example.pc.flatmaps.Views.MyProfileFragment;
import com.example.pc.flatmaps.Views.SearchFragment;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.AndroidInjection;
import dagger.android.AndroidInjector;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

public class MainActivity extends AppCompatActivity implements HasSupportFragmentInjector, NavigationView.OnNavigationItemSelectedListener, DrawerInterface
{
    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Inject
    Boolean registered;

    @BindView(R.id.toolbar_main)
    Toolbar toolbar;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    private Unbinder unbinder;
    private Drawable toolbarDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AndroidInjection.inject(this);
        unbinder = ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        addDrawerListener();

        addNavigationListener();

        if(!isItConfigurationChange(savedInstanceState))
            startNewFragment();
    }

    private boolean isItConfigurationChange(Bundle savedInstanceState)
    {
        return (savedInstanceState != null);
    }

    private void startNewFragment()
    {
        Fragment newFragment;

        if(isUserRegistered())
            newFragment = new MapsFragment();

        else
            newFragment = new LoginFragment();

        Manager.startNewFragment(getSupportFragmentManager(), newFragment);
    }

    private boolean isUserRegistered()
    {
        return (registered);
    }

    private void addNavigationListener()
    {
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void addDrawerListener()
    {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer.addDrawerListener(toggle);

        toggle.syncState();
    }

    @Override
    public AndroidInjector<Fragment> supportFragmentInjector()
    {
        return dispatchingAndroidInjector;
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onBackPressed()
    {
        if(drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);

        else
            super.onBackPressed();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        Fragment newFragment = null;

        switch (id){
            case R.id.map:
                newFragment = new MapsFragment();
                break;
            case R.id.search:
                newFragment = new SearchFragment();
                break;
            case R.id.my_profile:
                newFragment = new MyProfileFragment();
                break;
            case R.id.my_adds:
                newFragment = new MyAdsFragment();
                break;
        }

        Manager.startNewFragment(getSupportFragmentManager(), newFragment, null);

        drawer.closeDrawer(GravityCompat.START);

        return false;
    }

    @Override
    public void lockDrawer()
    {
        if(toolbarDrawable == null)
            toolbarDrawable = toolbar.getNavigationIcon();

        toolbar.setNavigationIcon(null);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    @Override
    public void unlockDrawer()
    {
        toolbar.setNavigationIcon(toolbarDrawable);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }
}

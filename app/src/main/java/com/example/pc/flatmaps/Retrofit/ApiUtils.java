package com.example.pc.flatmaps.Retrofit;

import android.content.Context;

import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;

public class ApiUtils
{
    private static final String URL_TO_API = "http://www.apkawwsis.nstrefa.pl/aplikacja/php/";

    public static UserInterface getUserInterface(Context context)
    {
        return RetrofitClient.getClient(URL_TO_API, context).create(UserInterface.class);
    }
}

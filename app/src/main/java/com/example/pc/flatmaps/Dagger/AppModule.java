package com.example.pc.flatmaps.Dagger;

import com.example.pc.flatmaps.Interfaces.AddAdsFragment.AddAdsFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.AddAdsFragment.AddAdsFragmentView;
import com.example.pc.flatmaps.Interfaces.DetailFragment.DetailFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.DetailFragment.DetailFragmentView;
import com.example.pc.flatmaps.Interfaces.EditAdsFragment.EditAdsFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.EditAdsFragment.EditAdsFragmentView;
import com.example.pc.flatmaps.Interfaces.LoginFragment.LoginFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.LoginFragment.LoginFragmentView;
import com.example.pc.flatmaps.Interfaces.MapFragment.MapFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.MapFragment.MapFragmentView;
import com.example.pc.flatmaps.Interfaces.MyAdsFragment.MyAdsFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.MyAdsFragment.MyAdsFragmentView;
import com.example.pc.flatmaps.Interfaces.MyProfile.MyProfileFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.MyProfile.MyProfileFragmentView;
import com.example.pc.flatmaps.Interfaces.RegisterFragment.RegisterFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.RegisterFragment.RegisterFragmentView;
import com.example.pc.flatmaps.Interfaces.SearchFragment.SearchFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.SearchFragment.SearchFragmentView;
import com.example.pc.flatmaps.Presenters.AddAdsFragmentPresenterImpl;
import com.example.pc.flatmaps.Presenters.DetailFragmentPresenterImpl;
import com.example.pc.flatmaps.Presenters.EditAdsFragmentPresenterImpl;
import com.example.pc.flatmaps.Presenters.MyAdsFragmentPresenterImpl;
import com.example.pc.flatmaps.Presenters.MyProfileFragmentPresenterImpl;
import com.example.pc.flatmaps.Presenters.RegisterFragmentPresenterImpl;
import com.example.pc.flatmaps.Presenters.SearchFragmentPresenterImpl;
import com.example.pc.flatmaps.Views.Activities.MainActivity;
import com.example.pc.flatmaps.Presenters.LoginFragmentPresenterImpl;
import com.example.pc.flatmaps.Presenters.MapFragmentPresenterImpl;
import com.example.pc.flatmaps.Views.AddAdsFragment;
import com.example.pc.flatmaps.Views.DetailFragment;
import com.example.pc.flatmaps.Views.EditAdsFragment;
import com.example.pc.flatmaps.Views.MyAdsFragment;
import com.example.pc.flatmaps.Views.RegisterFragment;
import com.example.pc.flatmaps.Views.LoginFragment;
import com.example.pc.flatmaps.Views.MapsFragment;
import com.example.pc.flatmaps.Views.MyProfileFragment;
import com.example.pc.flatmaps.Views.SearchFragment;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class AppModule
{
    @ContributesAndroidInjector
    abstract MainActivity contributeActivityInjector();

    @ContributesAndroidInjector
    abstract LoginFragment contributeLoginFragmentInjector();

    @ContributesAndroidInjector
    abstract MapsFragment contributeMapFragmentInjector();

    @ContributesAndroidInjector
    abstract MyProfileFragment contributeMyProfileFragmentInjector();

    @ContributesAndroidInjector
    abstract RegisterFragment contributeRegisterFragmentInjector();

    @ContributesAndroidInjector
    abstract AddAdsFragment contributeAddAdsFragmentInjector();

    @ContributesAndroidInjector
    abstract SearchFragment contributeSearchFragmentInjector();

    @ContributesAndroidInjector
    abstract DetailFragment contributeDetailFragmentInjector();

    @ContributesAndroidInjector
    abstract MyAdsFragment contributeMyAdsFragmentInjector();

    @ContributesAndroidInjector
    abstract EditAdsFragment contributeEditAdsFragmentInjector();

    @Binds
    abstract LoginFragmentView bindMainFragmentView(LoginFragment loginFragment);

    @Binds
    abstract LoginFragmentPresenter bindMainFragmentPresenter(LoginFragmentPresenterImpl loginFragmentPresenterImpl);

    @Binds
    abstract MapFragmentView bindMapFragmentView(MapsFragment mapsFragment);

    @Binds
    abstract MapFragmentPresenter bindMapFragmentPresenter(MapFragmentPresenterImpl mapFragmentPresenterImpl);

    @Binds
    abstract MyProfileFragmentView bindMyProfileFragmentView(MyProfileFragment myProfileFragment);

    @Binds
    abstract MyProfileFragmentPresenter bindMyProfileFragmentPresenter(MyProfileFragmentPresenterImpl myProfileFragmentPresenter);

    @Binds
    abstract RegisterFragmentView bindRegisterFragmentView(RegisterFragment registerFragment);

    @Binds
    abstract RegisterFragmentPresenter bindRegisterFragmentPresenter(RegisterFragmentPresenterImpl registerFragmentPresenter);

    @Binds
    abstract AddAdsFragmentView bindAddAdsFragmentView(AddAdsFragment addAdsFragment);

    @Binds
    abstract AddAdsFragmentPresenter bindAddAdsFragmentPresenter(AddAdsFragmentPresenterImpl addAdsFragmentPresenter);

    @Binds
    abstract SearchFragmentView bindSearchFragmentView(SearchFragment searchFragment);

    @Binds
    abstract SearchFragmentPresenter bindSearchFragmentPresenter(SearchFragmentPresenterImpl searchFragmentPresenter);

    @Binds
    abstract DetailFragmentView bindDetailFragmentView(DetailFragment detailFragment);

    @Binds
    abstract DetailFragmentPresenter bindDetailFragmentPresenter(DetailFragmentPresenterImpl detailFragmentPresenter);

    @Binds
    abstract MyAdsFragmentView bindMyAdsFragmentView(MyAdsFragment myAdsFragment);

    @Binds
    abstract MyAdsFragmentPresenter bindMyAdsFragmentPresenter(MyAdsFragmentPresenterImpl myAdsFragmentPresenter);

    @Binds
    abstract EditAdsFragmentView bindEditAdsFragmentView(EditAdsFragment editAdsFragment);

    @Binds
    abstract EditAdsFragmentPresenter bindEditAdsFragmentPresenter(EditAdsFragmentPresenterImpl editAdsFragmentPresenter);
}
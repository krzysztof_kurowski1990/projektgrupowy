package com.example.pc.flatmaps.Retrofit.Interfaces;

import com.example.pc.flatmaps.Models.Requests.AddAdsRequest;
import com.example.pc.flatmaps.Models.Requests.EditAdsRequest;
import com.example.pc.flatmaps.Models.Requests.LoginRequest;
import com.example.pc.flatmaps.Models.Requests.RegisterRequest;
import com.example.pc.flatmaps.Models.Requests.MyProfileRequest;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.Models.Responses.MyProfileResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import rx.Observable;

public interface UserInterface
{
    @POST("register.php")
    @Headers("Content-Type: application/json")
    Observable<Response<Void>> saveAccount(@Body RegisterRequest registerRequest);

    @POST("login.php")
    @Headers("Content-Type: application/json")
    Observable<Response<Void>> postLogin(@Body LoginRequest loginRequest);

    @GET("my_account_info.php")
    Observable<MyProfileResponse> getMyProfile(@Query("login") String login);

    @POST("edit_my_account.php")
    @Headers("Content-Type: application/json")
    Observable<Response<Void>> saveMyProfile(@Body MyProfileRequest myProfileRequest);

    @POST("new_offer.php")
    @Multipart
    Observable<Response<Void>> addAds(@Part MultipartBody.Part file,
                                      @Part("json") AddAdsRequest request);

    @GET("all.php")
    Observable<List<AdsResponse>> getAds();

    @GET("select.php")
    Observable<List<AdsResponse>> getFilterAds(@Query("city") String city,
                                               @Query("street") String street,
                                               @Query("range") String range,
                                               @Query("numberOfRooms") String numberOfRooms,
                                               @Query("price") String price);

    @GET("moje_ogloszenia.php")
    Observable<List<AdsResponse>> getMyAds(@Query("login") String login);

    @DELETE("delete_offer.php")
    Observable<Response<Void>> deleteAds(@Query("id") int id);

    @POST("edit.php")
    @Multipart
    Observable<Response<Void>> editAds(@Part MultipartBody.Part file,
                                       @Part("json") EditAdsRequest request);
}
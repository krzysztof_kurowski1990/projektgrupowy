package com.example.pc.flatmaps.Validator;

public class InputValidator
{
    public static String errorMessage;

    public static boolean isLoginValid(String login)
    {
        if(!isLoginCorrect(login))
        {
            errorMessage = "Login is incorrect";
            return false;
        }

        return true;
    }

    private static boolean isLoginCorrect(String login)
    {
        return (!login.isEmpty() && login.length() > 3);
    }

    public static boolean arePasswordsValid(String password1, String password2)
    {
        if(!isInputCorrect(password1, password2))
            return false;

        if(!arePasswordsTheSame(password1, password2))
        {
            errorMessage = "Passwords are different";
            return false;
        }

        if(!isPasswordLengthCorrect(password1, password2))
        {
            errorMessage = "Password must has at least 3 characters";
            return false;
        }

        return true;
    }

    private static boolean arePasswordsTheSame(String password1, String password2)
    {
        return (password1.equals(password2));
    }

    private static boolean isPasswordLengthCorrect(String password1, String password2)
    {
        return (password1.length() > 3);
    }

    public static boolean isInputCorrect(String... inputTexts)
    {
        for(String inputText : inputTexts)
        {
            if(inputText.isEmpty()) {
                errorMessage = "Input cant be empty";
                return false;
            }
        }

        return true;
    }
}

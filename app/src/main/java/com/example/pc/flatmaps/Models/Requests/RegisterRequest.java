package com.example.pc.flatmaps.Models.Requests;

public class RegisterRequest
{
    private String login;
    private String password;
    private String password2;

    public RegisterRequest(String login, String password, String password2)
    {
        this.login = login;
        this.password = password;
        this.password2 = password2;
    }

    public String getLogin()
    {
        return login;
    }

    public void setLogin(String login)
    {
        this.login = login;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }
}

package com.example.pc.flatmaps.Interfaces.MapFragment;

import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public interface MapFragmentView
{
    public void showToast(String message);
    public void addMarker(MarkerOptions markerOptions);
    public void onMarkerSelected(AdsResponse ads);
}

package com.example.pc.flatmaps.Presenters;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.MyAdsFragment.MyAdsFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.MyAdsFragment.MyAdsFragmentView;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;

import java.util.List;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MyAdsFragmentPresenterImpl implements MyAdsFragmentPresenter
{
    public static final String NO_ADS_RESPONSE_CODE = "410";

    private MyAdsFragmentView mFragmentView;
    private UserInterface mUserInterface;
    private Subscriber<AdsResponse> mAdsSubscriber;
    private Observable<List<AdsResponse>> mAdsObservable;
    private Subscriber<Response<Void>> mDeleteAdsSubscriber;
    private Observable<Response<Void>> mDeleteAdsObservable;

    @Inject
    public MyAdsFragmentPresenterImpl(MyAdsFragmentView myAdsFragmentView, UserInterface userInterface)
    {
        mFragmentView = myAdsFragmentView;
        mUserInterface = userInterface;
    }

    @Override
    public void onStart(String userLogin)
    {
        getMyAds(userLogin);
    }

    private void getMyAds(String userLogin)
    {
        createGetAdsSubsciber();

        createGetAdsObservable(userLogin);

        getAdsSubscribe();
    }

    private void getAdsSubscribe()
    {
        mAdsObservable
                .flatMap(Observable::from)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(mAdsSubscriber);
    }

    private void createGetAdsObservable(String userLogin)
    {
        mAdsObservable = mUserInterface.getMyAds(userLogin);
    }

    private void createGetAdsSubsciber()
    {
        mAdsSubscriber = new Subscriber<AdsResponse>() {
            @Override
            public void onCompleted() {}

            @Override
            public void onError(Throwable e)
            {
                if(!isItNoAdsError(e.getMessage()))
                    mFragmentView.showToast(e.getMessage());
            }

            @Override
            public void onNext(AdsResponse adsResponse)
            {
                mFragmentView.addAds(adsResponse);
            }
        };
    }

    private boolean isItNoAdsError(String message)
    {
        return (message.contains(NO_ADS_RESPONSE_CODE));
    }

    @Override
    public void onDestroy()
    {
        Manager.unSubscribe(mAdsSubscriber, mDeleteAdsSubscriber);
    }

    @Override
    public void deleteAds(AdsResponse ads)
    {
        createDeleteSubscriber(ads);

        createDeleteObservable(ads);

        subscribeDelete();
    }

    private void createDeleteObservable(AdsResponse ads)
    {
        int adsId = ads.getId_ogloszenia();

        mDeleteAdsObservable = mUserInterface.deleteAds(adsId);
    }

    private void subscribeDelete()
    {
        mDeleteAdsObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(mDeleteAdsSubscriber);
    }

    private void createDeleteSubscriber(AdsResponse ads)
    {
        mDeleteAdsSubscriber = new Subscriber<Response<Void>>() {
            @Override
            public void onCompleted() {}

            @Override
            public void onError(Throwable e)
            {
                mFragmentView.showToast(e.getMessage());
            }

            @Override
            public void onNext(Response<Void> response)
            {
                if(Manager.isResponseSuccessfull(response))
                    mFragmentView.onSuccessDeleteAds(ads);
            }
        };
    }
}

package com.example.pc.flatmaps.Views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.example.pc.flatmaps.HelpClassess.Keys;
import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.DrawerInterface;
import com.example.pc.flatmaps.Interfaces.LoginFragment.LoginFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.LoginFragment.LoginFragmentView;
import com.example.pc.flatmaps.Models.Requests.LoginRequest;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.Views.Activities.MainActivity;
import com.example.pc.flatmaps.databinding.FragmentLoginBinding;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

public class LoginFragment extends Fragment implements LoginFragmentView
{
    @Inject
    LoginFragmentPresenter mPresenter;

    @Inject
    String userLogin;

    @Inject
    Context mContext;

    @BindView(R.id.etLogin)
    EditText etLogin;

    @BindView(R.id.etPassword)
    EditText etPassword;

    private FragmentLoginBinding binding;
    private Unbinder unbinder;
    private DrawerInterface drawerInterface;

    @Inject
    public LoginFragment(){}

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        if(context instanceof MainActivity)
        {
            drawerInterface  = (MainActivity) context;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        AndroidSupportInjection.inject(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, parent, false);
        binding.setFragment(this);

        unbinder = ButterKnife.bind(this, binding.getRoot());

        drawerInterface.lockDrawer();

        return binding.getRoot();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        unbinder.unbind();

        binding.unbind();

        mPresenter.onDestroy();

        drawerInterface.unlockDrawer();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.onStart();
    }

    public String getUserLogin()
    {
        return userLogin;
    }

    public void onViewCreated(View view, Bundle savedInstanceState){}

    public void btnLoginClick(View view)
    {
        LoginRequest loginRequest = new LoginRequest(getLogin(), getPassword());
        mPresenter.postLogin(loginRequest);
    }

    private String getPassword()
    {
        return Manager.getText(etPassword);
    }

    private String getLogin()
    {
        return Manager.getText(etLogin);
    }

    public void tvRegisterClick(View view)
    {
        Manager.startNewFragment(getFragmentManager(), new RegisterFragment(), null);
    }

    @Override
    public void successfullLogin()
    {
        Manager.saveInSp(mContext, Keys.REGISTER, true);

        Manager.saveInSp(mContext, Keys.LOGIN, getLogin());

        Manager.hideKeyboard(getActivity());

        Manager.startNewFragment(getFragmentManager(), new MapsFragment());
    }

    @Override
    public void showToast(String errorMessage)
    {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
    }
}

package com.example.pc.flatmaps.Models.Requests;

public class FilterAdsRequest
{
    private String city;
    private String street;
    private String range;
    private String numberOfRooms;
    private String size;
    private String price;

    public FilterAdsRequest(String city, String street, String range, String numberOfRooms, String size, String price)
    {
        this.city = city;
        this.street = street;
        this.range = range;
        this.numberOfRooms = numberOfRooms;
        this.size = size;
        this.price = price;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getRange() {
        return range;
    }

    public void setRange(String range) {
        this.range = range;
    }

    public String getNumberOfRooms() {
        return numberOfRooms;
    }

    public void setNumberOfRooms(String numberOfRooms) {
        this.numberOfRooms = numberOfRooms;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}

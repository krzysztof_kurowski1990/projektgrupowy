package com.example.pc.flatmaps.Views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.MapFragment.MapFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.MapFragment.MapFragmentView;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.databinding.FragmentMapBinding;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

import static com.example.pc.flatmaps.Views.SearchFragment.FILTER_ADS;

public class MapsFragment extends Fragment implements MapFragmentView, OnMapReadyCallback, GoogleMap.OnMarkerClickListener
{
    @Inject
    MapFragmentPresenter mPresenter;

    @Inject
    LatLngBounds.Builder builder;

    @Inject
    Context mContext;

    public static String ADS = "ads";
    private static int ZOOM_SIZE = 50;

    private GoogleMap mMap;
    private FragmentMapBinding binding;
    private LatLngBounds bounds;
    private Bundle bundle;

    private List<AdsResponse> getFilterAds()
    {
        bundle = this.getArguments();

        if(bundle != null) {
            String adsString = bundle.getString(FILTER_ADS);

            AdsResponse[] adsTable = new Gson().fromJson(adsString, AdsResponse[].class);

            return Arrays.asList(adsTable);
        }

        else
            return new ArrayList<>();
    }

    @Inject
    public MapsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        int title = R.string.title_activity_maps;
        Manager.changeActionBarTitle(getActivity(), title);

        AndroidSupportInjection.inject(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_map, parent, false);
        binding.setFragment(this);

        return binding.getRoot();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        mPresenter.onDestroy();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.onStart();
    }


    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        loadMap();
    }

    private void loadMap()
    {
        FragmentManager fm = getChildFragmentManager();

        SupportMapFragment supportMapFragment = SupportMapFragment.newInstance();

        fm.beginTransaction().replace(R.id.mapPlaceholder, supportMapFragment).commit();

        supportMapFragment.getMapAsync(this);
    }

    @Override
    public void showToast(String message)
    {
        Manager.showToast(mContext, message);
    }

    @Override
    public void addMarker(MarkerOptions markerOptions)
    {
        Marker newMarker = mMap.addMarker(markerOptions);
        LatLng markerPosition = newMarker.getPosition();

        centerMap(markerPosition);
    }

    private void centerMap(LatLng markerPosition)
    {
        builder.include(markerPosition);

        bounds = builder.build();

        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, ZOOM_SIZE));
    }

    @Override
    public void onMapReady(GoogleMap googleMap)
    {
        mMap = googleMap;

        mPresenter.getAds(getFilterAds());

        personalizeMap(mMap);
    }

    private void personalizeMap(GoogleMap mMap)
    {
        mMap.getUiSettings().setAllGesturesEnabled(true);

        mMap.setOnMarkerClickListener(this);
    }

    public void fabClick(View view)
    {
        Manager.startNewFragment(getFragmentManager(), new AddAdsFragment(), null);
    }

    @Override
    public boolean onMarkerClick(Marker marker)
    {
        mPresenter.onMarkerClick(marker);

        return false;
    }

    @Override
    public void onMarkerSelected(AdsResponse adsResponse)
    {
        startDetailFragment(adsResponse);
    }

    private void startDetailFragment(AdsResponse choosenAds)
    {
        Bundle bundle = new Bundle();
        String ads = new Gson().toJson(choosenAds);
        bundle.putString(ADS, ads);

        DetailFragment detailFragment = new DetailFragment();
        detailFragment.setArguments(bundle);

        Manager.startNewFragment(getFragmentManager(), detailFragment, null);
    }
}

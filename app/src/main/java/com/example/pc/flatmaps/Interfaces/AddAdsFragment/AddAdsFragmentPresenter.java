package com.example.pc.flatmaps.Interfaces.AddAdsFragment;

import android.graphics.Bitmap;

import com.example.pc.flatmaps.Models.Requests.AddAdsRequest;

import java.io.File;

public interface AddAdsFragmentPresenter
{
    public void onStart();
    public void onDestroy();
    public void onPause();
    public void onResume();
    public void addNewAds(File file, AddAdsRequest addAdsRequest);
    public void createBase64FromBitmap(Bitmap bitmap);
}

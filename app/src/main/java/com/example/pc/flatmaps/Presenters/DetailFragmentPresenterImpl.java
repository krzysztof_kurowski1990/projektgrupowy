package com.example.pc.flatmaps.Presenters;

import com.example.pc.flatmaps.Interfaces.DetailFragment.DetailFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.DetailFragment.DetailFragmentView;

import javax.inject.Inject;

public class DetailFragmentPresenterImpl implements DetailFragmentPresenter
{
    private DetailFragmentView mFragmentView;

    @Inject
    public DetailFragmentPresenterImpl(DetailFragmentView detailFragmentView)
    {
        mFragmentView = detailFragmentView;
    }

    @Override
    public void onStart() {}

    @Override
    public void onDestroy()
    {
    }
}

package com.example.pc.flatmaps.Views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.pc.flatmaps.Adapters.MyAdsAdapter;
import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.MyAdsAdapter.MyAdsClickListener;
import com.example.pc.flatmaps.Interfaces.MyAdsFragment.MyAdsFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.MyAdsFragment.MyAdsFragmentView;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.databinding.FragmentMyAdsBinding;
import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

public class MyAdsFragment extends Fragment implements MyAdsFragmentView, MyAdsClickListener
{
    @Inject
    MyAdsFragmentPresenter mPresenter;

    @Inject
    String userLogin;

    @Inject
    Context mContext;

    @BindView(R.id.rvMyAds)
    RecyclerView rvMyAds;

    public static String ADS = "ads";

    private FragmentMyAdsBinding binder;
    private MyAdsAdapter adapter;
    private Unbinder unbinder;

    @Inject
    public MyAdsFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        int title = R.string.my_ads_fragment_title;
        Manager.changeActionBarTitle(getActivity(), title);

        AndroidSupportInjection.inject(this);

        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_my_ads, parent, false);
        binder.setFragment(this);

        unbinder = ButterKnife.bind(this, binder.getRoot());

        return binder.getRoot();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        binder.unbind();

        unbinder.unbind();

        mPresenter.onDestroy();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.onStart(userLogin);
    }

    @Override
    public void addAds(AdsResponse adsResponse)
    {
        adapter.addAds(adsResponse);
    }

    @Override
    public void showToast(String message)
    {
        Manager.showToast(mContext, message);
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        setupRecyclerView();
    }

    private void setupRecyclerView()
    {
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        rvMyAds.setLayoutManager(mLayoutManager);

        rvMyAds.setItemAnimator(new DefaultItemAnimator());

        setAdapter();
    }

    private void setAdapter()
    {
        adapter = new MyAdsAdapter(R.layout.row_my_ads_adapter, getActivity(), this);

        rvMyAds.setAdapter(adapter);
    }

    @Override
    public void onSuccessDeleteAds(AdsResponse ads)
    {
        adapter.removeAds(ads);
    }

    @Override
    public void delete(AdsResponse ads)
    {
        mPresenter.deleteAds(ads);
    }

    @Override
    public void edit(AdsResponse ads)
    {
        Bundle bundle = new Bundle();
        String adsJson = new Gson().toJson(ads);
        bundle.putString(ADS, adsJson);

        EditAdsFragment editFragment = new EditAdsFragment();
        editFragment.setArguments(bundle);

        Manager.startNewFragment(getFragmentManager(), editFragment, null);
    }
}

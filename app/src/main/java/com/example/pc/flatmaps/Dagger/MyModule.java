package com.example.pc.flatmaps.Dagger;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.example.pc.flatmaps.HelpClassess.Keys;
import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Retrofit.ApiUtils;
import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;
import com.google.android.gms.maps.model.LatLngBounds;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class MyModule
{
    private Context mContext;

    public MyModule(Application application)
    {
        mContext = application;
    }

    @Provides
    @Singleton
    UserInterface provideUserInterface()
    {
        return ApiUtils.getUserInterface(mContext);
    }

    @Provides
    @Singleton
    Context provideContext()
    {
        return mContext;
    }

    @Provides
    String provideLogin()
    {
        return Manager.getFromSp(mContext, Keys.LOGIN);
    }

    @Provides
    Boolean provideRegister()
    {
        return Manager.getBooleanFromSp(mContext, Keys.REGISTER);
    }

    @Provides
    LatLngBounds.Builder provideBuilder(){
        return new LatLngBounds.Builder();
    }
}
package com.example.pc.flatmaps.Retrofit;

import android.content.Context;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient
{
    private static Retrofit retrofit;

    public static Retrofit getClient(String URL_TO_API, Context context)
    {
        if(retrofit == null)
            retrofit = new Retrofit.Builder()
                    .baseUrl(URL_TO_API)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(OkHttpInterceptor.getClient(context))
                    .build();

        return retrofit;
    }
}

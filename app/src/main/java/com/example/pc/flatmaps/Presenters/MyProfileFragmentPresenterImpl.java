package com.example.pc.flatmaps.Presenters;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.MyProfile.MyProfileFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.MyProfile.MyProfileFragmentView;
import com.example.pc.flatmaps.Models.Requests.MyProfileRequest;
import com.example.pc.flatmaps.Models.Responses.MyProfileResponse;
import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;
import com.example.pc.flatmaps.Validator.InputValidator;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MyProfileFragmentPresenterImpl implements MyProfileFragmentPresenter
{
    private MyProfileFragmentView mFragmentView;
    private UserInterface mUserInterface;
    private Subscriber<MyProfileResponse> mMyProfileSubscriber;
    private Observable<MyProfileResponse> mMyProfileObservable;

    private Subscriber<Response<Void>> saveMyProfileSubscriber;
    private Observable<Response<Void>> saveMyProfileObservable;

    @Inject
    public MyProfileFragmentPresenterImpl(MyProfileFragmentView myProfileFragmentView, UserInterface userInterface)
    {
        mFragmentView = myProfileFragmentView;
        mUserInterface = userInterface;
    }

    @Override
    public void onStart(String userLogin)
    {
        getMyProfile(userLogin);
    }

    @Override
    public void onDestroy()
    {
        Manager.unSubscribe(saveMyProfileSubscriber, mMyProfileSubscriber);
    }

    @Override
    public void postMyProfile(MyProfileRequest myProfileRequest)
    {
        if(!isRequestValid(myProfileRequest))
        {
            mFragmentView.showToast(InputValidator.errorMessage);
            return;
        }

        saveMyProfileSubscriber();

        saveMyProfileObservable(myProfileRequest);

        saveMyProfileSubscribe();
    }

    private boolean isRequestValid(MyProfileRequest myProfileRequest)
    {
        return (InputValidator.isInputCorrect(
                myProfileRequest.getEmail(),
                myProfileRequest.getImie(),
                myProfileRequest.getLogin(),
                myProfileRequest.getNazwisko(),
                myProfileRequest.getNr_tel()));
    }

    private void saveMyProfileSubscribe()
    {
        saveMyProfileObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(saveMyProfileSubscriber);
    }

    private void saveMyProfileObservable(MyProfileRequest myProfileRequest)
    {
        saveMyProfileObservable = mUserInterface.saveMyProfile(myProfileRequest);
    }

    private void saveMyProfileSubscriber()
    {
        saveMyProfileSubscriber = new Subscriber<Response<Void>>() {
            @Override
            public void onCompleted() {}

            @Override
            public void onError(Throwable e)
            {
                mFragmentView.showToast(e.getMessage());
            }

            @Override
            public void onNext(Response<Void> response)
            {
                if(Manager.isResponseSuccessfull(response))
                    mFragmentView.onSuccessSaveProfile();
            }
        };
    }

    private void getMyProfile(String userLogin)
    {
        myProfileSubscriber();

        myProfileObservable(userLogin);

        myProfileSubscribe();
    }

    private void myProfileSubscriber()
    {
        mMyProfileSubscriber = new Subscriber<MyProfileResponse>() {
            @Override
            public void onCompleted() {}

            @Override
            public void onError(Throwable e)
            {
                mFragmentView.showToast(e.getMessage());
            }

            @Override
            public void onNext(MyProfileResponse myProfileResponse) {
               mFragmentView.bindMyProfile(myProfileResponse);
            }
        };
    }

    private void myProfileObservable(String userLogin)
    {
        mMyProfileObservable = mUserInterface.getMyProfile(userLogin);
    }

    private void myProfileSubscribe()
    {
        mMyProfileObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(mMyProfileSubscriber);
    }
}

package com.example.pc.flatmaps.Presenters;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.RegisterFragment.RegisterFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.RegisterFragment.RegisterFragmentView;
import com.example.pc.flatmaps.Models.Requests.RegisterRequest;
import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;
import com.example.pc.flatmaps.Validator.InputValidator;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegisterFragmentPresenterImpl implements RegisterFragmentPresenter
{
    private RegisterFragmentView mFragmentView;
    private UserInterface mUserInterface;
    private Subscriber<Response<Void>> registerSubscriber;
    private Observable<Response<Void>> registerObservable;

    @Inject
    public RegisterFragmentPresenterImpl(RegisterFragmentView registerFragmentView, UserInterface userInterface)
    {
        mFragmentView = registerFragmentView;
        mUserInterface = userInterface;
    }

    @Override
    public void onStart() {}

    @Override
    public void onDestroy()
    {
        Manager.unSubscribe(registerSubscriber);
    }

    @Override
    public void saveAccount(RegisterRequest registerRequest)
    {
        if(isRequestValidate(registerRequest))
        {
            registerSubscriber();

            registerObservable(registerRequest);

            registerSubscribe();
        }

        else
            mFragmentView.showToast(InputValidator.errorMessage);
    }

    private boolean isRequestValidate(RegisterRequest request)
    {
        String login = request.getLogin();

        if(!InputValidator.isLoginValid(login))
            return false;

        String password1 = request.getPassword();
        String password2 = request.getPassword2();

        if(!InputValidator.arePasswordsValid(password1, password2))
            return false;

        return true;
    }

    private void registerSubscribe()
    {
        registerObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(registerSubscriber);
    }

    private void registerObservable(RegisterRequest registerRequest)
    {
        registerObservable = mUserInterface.saveAccount(registerRequest);
    }

    private void registerSubscriber()
    {
        registerSubscriber = new Subscriber<Response<Void>>() {
            @Override
            public void onCompleted() {}

            @Override
            public void onError(Throwable e)
            {
                mFragmentView.showToast(e.getMessage());
            }

            @Override
            public void onNext(Response<Void> response)
            {
                if(Manager.isResponseSuccessfull(response))
                    mFragmentView.successSaveAccount();
            }
        };
    }
}

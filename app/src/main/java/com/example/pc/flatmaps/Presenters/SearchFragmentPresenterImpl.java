package com.example.pc.flatmaps.Presenters;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.SearchFragment.SearchFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.SearchFragment.SearchFragmentView;
import com.example.pc.flatmaps.Models.Requests.FilterAdsRequest;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;
import com.example.pc.flatmaps.Validator.InputValidator;

import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class SearchFragmentPresenterImpl implements SearchFragmentPresenter
{
    private SearchFragmentView mFragmentView;
    private UserInterface mUserInterface;
    private Subscriber<List<AdsResponse>> mFilterAdsSubscriber;
    private Observable<List<AdsResponse>> mFilterAdsObservable;

    @Inject
    public SearchFragmentPresenterImpl(SearchFragmentView searchFragmentView, UserInterface userInterface)
    {
        mFragmentView = searchFragmentView;
        mUserInterface = userInterface;
    }

    @Override
    public void onStart()
    {}

    @Override
    public void onDestroy()
    {
        Manager.unSubscribe(mFilterAdsSubscriber);
    }

    @Override
    public void getFilterAds(FilterAdsRequest request)
    {
        if(!isRequestValidate(request))
        {
            mFragmentView.showToast(InputValidator.errorMessage);
            return;
        }

        createSubsciber();

        creteObservable(request);

        filterAdsSubscribe();
    }

    private boolean isRequestValidate(FilterAdsRequest request)
    {
        return (InputValidator.isInputCorrect(
                request.getCity(),
                request.getNumberOfRooms(),
                request.getPrice(),
                request.getRange(),
                request.getSize(),
                request.getStreet()));
    }

    private void createSubsciber()
    {
        mFilterAdsSubscriber = new Subscriber<List<AdsResponse>>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                mFragmentView.showToast(e.getMessage());
            }

            @Override
            public void onNext(List<AdsResponse> adsResponses)
            {
                mFragmentView.onSuccessGetAds(adsResponses);
            }
        };
    }

    private void creteObservable(FilterAdsRequest request)
    {
        String city = request.getCity();
        String street = request.getStreet();
        String range = request.getRange();
        String numberOfRooms = request.getNumberOfRooms();
        String price = request.getPrice();

        mFilterAdsObservable = mUserInterface.getFilterAds(city, street, range, numberOfRooms, price);
    }

    private void filterAdsSubscribe()
    {
        mFilterAdsObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(mFilterAdsSubscriber);
    }
}

package com.example.pc.flatmaps.Interfaces;

public interface DrawerInterface
{
    public void lockDrawer();
    public void unlockDrawer();
}

package com.example.pc.flatmaps.Interfaces.MyAdsFragment;

import com.example.pc.flatmaps.Models.Responses.AdsResponse;

public interface MyAdsFragmentView
{
    public void showToast(String message);
    public void addAds(AdsResponse adsResponse);
    public void onSuccessDeleteAds(AdsResponse ads);
}

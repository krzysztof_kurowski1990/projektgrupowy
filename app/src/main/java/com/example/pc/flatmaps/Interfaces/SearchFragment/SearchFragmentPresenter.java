package com.example.pc.flatmaps.Interfaces.SearchFragment;

import com.example.pc.flatmaps.Models.Requests.FilterAdsRequest;

public interface SearchFragmentPresenter
{
    public void onStart();
    public void onDestroy();
    public void getFilterAds(FilterAdsRequest request);
}

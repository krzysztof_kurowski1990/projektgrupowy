package com.example.pc.flatmaps.HelpClassess;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.pc.flatmaps.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Response;
import rx.Subscriber;

public class Manager
{
    private static final String SHARED_PREFERENCES_NAME = "MySP";

    public static void changeActionBarTitle(Context context, int title)
    {
        ((AppCompatActivity) context).getSupportActionBar().setTitle(context.getResources().getString(title));
    }

    public static String getText(EditText editText)
    {
        return editText.getText().toString();
    }

    public static String getText(TextView textView)
    {
        return textView.getText().toString();
    }

    public static void startNewFragment(FragmentManager fragmentManager, Fragment newFragment, String backStack)
    {
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragmentPlaceholder, newFragment)
                .addToBackStack(backStack)
                .commit();
    }

    public static void startNewFragment(FragmentManager fragmentManager, Fragment newFragment)
    {
        fragmentManager
                .beginTransaction()
                .replace(R.id.fragmentPlaceholder, newFragment)
                .commit();
    }

    public static void showToast(Context context, String message)
    {
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

    public static void hideKeyboard(FragmentActivity activity)
    {
        View view = activity.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public static void saveInSp(Context context, String name, String value)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(name, value);
        editor.apply();
    }

    public static void saveInSp(Context context, String name, Boolean value)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(name, value);
        editor.apply();
    }

    public static String getFromSp(Context context, String name)
    {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String value = preferences.getString(name, "");

        return value;
    }

    public static Boolean getBooleanFromSp(Context context, String name)
    {
        SharedPreferences preferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        Boolean value = preferences.getBoolean(name, false);

        return value;
    }

    public static void clearSp(Context context)
    {
        SharedPreferences.Editor editor = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.clear();
        editor.apply();
    }

    public static boolean isResponseSuccessfull(Response<Void> response)
    {
        return (response.isSuccessful() && response.code() == 200);
    }

    public static void unSubscribe(Subscriber... subscribers)
    {
        for(Subscriber subscriber : subscribers)
            if(subscriber != null && subscriber.isUnsubscribed())
                subscriber.unsubscribe();
    }

    public static boolean isCameraPermissionGranted(Context context)
    {
        return (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    public static String convertBitmapToBase64(Bitmap bitmap)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

        byte[] bytes = baos.toByteArray();

        return Base64.encodeToString(bytes, Base64.NO_WRAP);
    }

    public static File getOutputFile(Context mContext) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        return File.createTempFile(imageFileName,".jpg", storageDir);
    }

    public static String[] getStringArray(Context mContext, int array)
    {
        return mContext.getResources().getStringArray(array);
    }
}

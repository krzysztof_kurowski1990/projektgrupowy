package com.example.pc.flatmaps.Interfaces.LoginFragment;

public interface LoginFragmentView
{
    public void successfullLogin();
    public void showToast(String message);
}

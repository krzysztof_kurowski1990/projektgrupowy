package com.example.pc.flatmaps.Retrofit;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkHttpInterceptor
{
    private static OkHttpClient okHttpClient;
    private static Interceptor interceptor;

    public static OkHttpClient getClient(Context context)
    {
        if(okHttpClient == null)
            return new OkHttpClient.Builder()
                    .addInterceptor(getInterceptor(context))
                    .build();

        else
            return okHttpClient;
    }

    private static Interceptor getInterceptor(Context context)
    {
        if(interceptor == null)
            return new Interceptor()
            {
                @Override
                public Response intercept(Chain chain) throws IOException
                {
                    Request request = chain.request();
                    Response response = chain.proceed(request);
                    int responseCode = response.code();

                    Log.e("request", request+"");
                    Log.e("response", response+"");
                    Log.e("responseCode", response.code()+"");
                    Log.e("responseMessage", response.message()+"");

                    if(responseCode != 200)
                        errorHandling(responseCode, context);

                    return response;
                }
            };

        else
            return interceptor;
    }

    private static void errorHandling(int responseCode, Context context) {
        String errorMessage = "Something goes wrong";

        switch (responseCode){
            case 400:
                errorMessage = "Username or password is incorrect";
                break;
            case 405:
                errorMessage = "The given login is already in use";
                break;
            case 410:
                errorMessage = "No ads";
                break;
        }

        String finalErrorMessage = errorMessage;

        runOnUiThread(() -> {
            Toast.makeText(context, finalErrorMessage, Toast.LENGTH_SHORT).show();
        }, context);
    }

    private static void runOnUiThread(Runnable runnable, Context context)
    {
        Handler handler = new Handler(context.getMainLooper());
        handler.post(runnable);
    }
}

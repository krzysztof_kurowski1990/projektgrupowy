package com.example.pc.flatmaps.Views;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.MyProfile.MyProfileFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.MyProfile.MyProfileFragmentView;
import com.example.pc.flatmaps.Models.Requests.MyProfileRequest;
import com.example.pc.flatmaps.Models.Responses.MyProfileResponse;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.Views.Activities.MainActivity;
import com.example.pc.flatmaps.databinding.FragmentMyProfileBinding;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

public class MyProfileFragment extends Fragment implements MyProfileFragmentView
{
    @Inject
    MyProfileFragmentPresenter mPresenter;

    @Inject
    Context mContext;

    @Inject
    String userLogin;

    @BindView(R.id.etName)
    EditText etName;

    @BindView(R.id.etSurname)
    EditText etSurname;

    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;

    @BindView(R.id.etEmail)
    EditText etEmail;

    private FragmentMyProfileBinding binding;
    private Unbinder unbinder;

    @Inject
    public MyProfileFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        int title = R.string.my_profile_fragment_title;

        Manager.changeActionBarTitle(getActivity(), title);

        AndroidSupportInjection.inject(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_profile, parent, false);
        binding.setFragment(this);

        unbinder = ButterKnife.bind(this, binding.getRoot());

        return binding.getRoot();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        unbinder.unbind();

        binding.unbind();

        mPresenter.onDestroy();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.onStart(userLogin);
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.my_profile, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        if (id == R.id.logout)
        {
            reset();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getUserLogin()
    {
        return userLogin;
    }

    private void reset()
    {
        Manager.clearSp(mContext);

        startNewActivity();
    }

    private void startNewActivity()
    {
        getActivity().finish();

        Intent mainActivity = new Intent(getActivity(), MainActivity.class);
        mainActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(mainActivity);
    }

    @Override
    public void bindMyProfile(MyProfileResponse myProfileResponse)
    {
        binding.setMyProfile(myProfileResponse);
    }

    @Override
    public void showToast(String message)
    {
        Manager.showToast(mContext, message);
    }

    @Override
    public void onSuccessSaveProfile()
    {
        showToast("Data has been updated");
    }

    public void btnSaveClick(View view)
    {
        MyProfileRequest myProfileRequest = new MyProfileRequest(userLogin, getName(), getSurname(), getPhoneNumber(), getEmail());
        mPresenter.postMyProfile(myProfileRequest);
    }

    private String getName()
    {
        return Manager.getText(etName);
    }

    private String getSurname()
    {
        return Manager.getText(etSurname);
    }

    private String getPhoneNumber()
    {
        return Manager.getText(etPhoneNumber);
    }

    private String getEmail()
    {
        return Manager.getText(etEmail);
    }
}

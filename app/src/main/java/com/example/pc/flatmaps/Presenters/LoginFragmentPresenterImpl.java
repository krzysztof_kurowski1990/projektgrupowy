package com.example.pc.flatmaps.Presenters;


import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.LoginFragment.LoginFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.LoginFragment.LoginFragmentView;
import com.example.pc.flatmaps.Models.Requests.LoginRequest;
import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;

import javax.inject.Inject;

import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LoginFragmentPresenterImpl implements LoginFragmentPresenter
{
    private LoginFragmentView mFragmentView;
    private UserInterface mUserInterface;
    private Subscriber<Response<Void>> mLoginSubscriber;
    private Observable<Response<Void>> mLoginObservable;

    @Inject
    public LoginFragmentPresenterImpl(LoginFragmentView loginFragmentView, UserInterface userInterface)
    {
        mFragmentView = loginFragmentView;
        mUserInterface = userInterface;

    }

    @Override
    public void onStart() {}

    @Override
    public void onDestroy()
    {
        Manager.unSubscribe(mLoginSubscriber);
    }

    @Override
    public void postLogin(LoginRequest loginRequest)
    {
            createSubsciber();

            createObservable(loginRequest);

            userSubscribe();
    }

    private void createSubsciber()
    {
        mLoginSubscriber = new Subscriber<Response<Void>>()
        {
            @Override
            public void onCompleted() {}

            @Override
            public void onError(Throwable e)
            {
                mFragmentView.showToast(e.getMessage());
            }

            @Override
            public void onNext(Response<Void> response)
            {
                if(Manager.isResponseSuccessfull(response))
                    mFragmentView.successfullLogin();
            }
        };
    }

    private void createObservable(LoginRequest loginRequest)
    {
        mLoginObservable = mUserInterface.postLogin(loginRequest);
    }

    private void userSubscribe()
    {
        mLoginObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(mLoginSubscriber);
    }
}

package com.example.pc.flatmaps.Interfaces.MyProfile;

import com.example.pc.flatmaps.Models.Responses.MyProfileResponse;

public interface MyProfileFragmentView
{
    public void bindMyProfile(MyProfileResponse myProfileResponse);
    public void showToast(String message);
    public void onSuccessSaveProfile();
}

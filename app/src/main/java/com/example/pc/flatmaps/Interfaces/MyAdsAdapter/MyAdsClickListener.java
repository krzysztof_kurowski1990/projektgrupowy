package com.example.pc.flatmaps.Interfaces.MyAdsAdapter;

import com.example.pc.flatmaps.Models.Responses.AdsResponse;

public interface MyAdsClickListener
{
    public void delete(AdsResponse ads);
    public void edit(AdsResponse ads);
    public void showToast(String message);
}

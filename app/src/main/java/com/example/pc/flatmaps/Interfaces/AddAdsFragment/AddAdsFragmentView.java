package com.example.pc.flatmaps.Interfaces.AddAdsFragment;

public interface AddAdsFragmentView
{
    public void showToast(String message);
    public void base64Created(String imageBase64);
    public void toggleSwipeLayout(boolean enabled);
}

package com.example.pc.flatmaps.Interfaces.SearchFragment;

import com.example.pc.flatmaps.Models.Responses.AdsResponse;

import java.util.List;

public interface SearchFragmentView
{
    public void showToast(String message);
    public void onSuccessGetAds(List<AdsResponse> adsResponses);
}

package com.example.pc.flatmaps.Interfaces.RegisterFragment;

public interface RegisterFragmentView
{
    public void showToast(String message);
    public void successSaveAccount();
}

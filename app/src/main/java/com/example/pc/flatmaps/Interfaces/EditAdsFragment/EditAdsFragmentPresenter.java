package com.example.pc.flatmaps.Interfaces.EditAdsFragment;

import android.graphics.Bitmap;

import com.example.pc.flatmaps.Models.Requests.EditAdsRequest;

import java.io.File;

public interface EditAdsFragmentPresenter
{
    public void onStart();
    public void onDestroy();
    public void onPause();
    public void onResume();
    public void createBase64FromBitmap(Bitmap bitmap);
    public void editAds(File imageFile, EditAdsRequest request);
}

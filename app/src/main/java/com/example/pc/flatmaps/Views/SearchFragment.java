package com.example.pc.flatmaps.Views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.SearchFragment.SearchFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.SearchFragment.SearchFragmentView;
import com.example.pc.flatmaps.Models.Requests.FilterAdsRequest;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.databinding.FragmentSearchBinding;
import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

public class SearchFragment extends Fragment implements SearchFragmentView
{
    @Inject
    SearchFragmentPresenter mPresenter;

    @Inject
    Context mContext;

    @BindView(R.id.etSearchCity)
    EditText etSearchCity;

    @BindView(R.id.etSearchStreet)
    EditText etSearchStreet;

    @BindView(R.id.spSearchNumberOfRooms)
    Spinner spSearchNumberOfRooms;

    @BindView(R.id.spSearchPrice)
    Spinner spSearchPrice;

    @BindView(R.id.spSearchSize)
    Spinner spSearchSize;

    @BindView(R.id.spSearchRange)
    Spinner spSearchRange;

    public static final String FILTER_ADS = "filter_ads";

    private FragmentSearchBinding binder;
    private Unbinder unbinder;

    private String getCity()
    {
        return etSearchCity.getText().toString();
    }

    private String getStreet()
    {
        return etSearchStreet.getText().toString();
    }

    private String getNumberOfRooms()
    {
        return spSearchNumberOfRooms.getSelectedItem().toString();
    }

    private String getSize()
    {
        return spSearchSize.getSelectedItem().toString();
    }

    private String getRange()
    {
        return spSearchRange.getSelectedItem().toString();
    }

    private String getPrice()
    {
        return spSearchPrice.getSelectedItem().toString();
    }

    @Inject
    public SearchFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        int title = R.string.search_fragment_title;
        Manager.changeActionBarTitle(getActivity(), title);

        AndroidSupportInjection.inject(this);

        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_search, parent, false);
        binder.setFragment(this);

        unbinder = ButterKnife.bind(this, binder.getRoot());

        return binder.getRoot();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        unbinder.unbind();

        binder.unbind();

        mPresenter.onDestroy();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.onStart();
    }

    @Override
    public void showToast(String message)
    {
        Manager.showToast(mContext, message);
    }

    @Override
    public void onSuccessGetAds(List<AdsResponse> adsResponses)
    {
        startMapFragment(adsResponses);
    }

    private void startMapFragment(List<AdsResponse> adsResponses)
    {
        Bundle bundle = new Bundle();
        String adsString = new Gson().toJson(adsResponses);
        bundle.putString(FILTER_ADS, adsString);

        MapsFragment mapsFragment = new MapsFragment();
        mapsFragment.setArguments(bundle);

        Manager.startNewFragment(getFragmentManager(), mapsFragment);
    }

    public void btnSearchClick(View view)
    {
        FilterAdsRequest request = new FilterAdsRequest(getCity(), getStreet(), getRange(), getNumberOfRooms(), getPrice(), getSize());
        mPresenter.getFilterAds(request);
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        setupSpiners();
    }

    private void setupSpiners()
    {
        setupSpRange();

        setupSpNumberOfRooms();

        setupSize();

        setupPrice();
    }

    private void setupPrice()
    {
        String[] priceList = Manager.getStringArray(mContext, R.array.priceList);

        spSearchPrice.setAdapter(getAdapter(priceList));
    }

    private void setupSize()
    {
        String[] sizeList = Manager.getStringArray(mContext, R.array.sizeList);

        spSearchSize.setAdapter(getAdapter(sizeList));
    }

    private void setupSpNumberOfRooms()
    {
        String[] roomTypesList = Manager.getStringArray(mContext, R.array.roomTypesList);

        spSearchNumberOfRooms.setAdapter(getAdapter(roomTypesList));
    }

    private void setupSpRange()
    {
        String[] rangeList = Manager.getStringArray(mContext, R.array.rangeList);

        spSearchRange.setAdapter(getAdapter(rangeList));
    }

    private SpinnerAdapter getAdapter(String[] itemList)
    {
        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.spinner_row_item, itemList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        return adapter;
    }
}

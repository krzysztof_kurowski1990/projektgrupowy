package com.example.pc.flatmaps.Interfaces.RegisterFragment;

import com.example.pc.flatmaps.Models.Requests.RegisterRequest;

public interface RegisterFragmentPresenter
{
    public void onStart();

    public void onDestroy();

    public void saveAccount(RegisterRequest registerRequest);
}

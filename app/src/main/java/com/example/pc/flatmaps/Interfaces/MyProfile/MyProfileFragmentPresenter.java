package com.example.pc.flatmaps.Interfaces.MyProfile;

import com.example.pc.flatmaps.Models.Requests.MyProfileRequest;

public interface MyProfileFragmentPresenter
{
    public void onStart(String userLogin);

    public void onDestroy();

    public void postMyProfile(MyProfileRequest myProfileRequest);
}

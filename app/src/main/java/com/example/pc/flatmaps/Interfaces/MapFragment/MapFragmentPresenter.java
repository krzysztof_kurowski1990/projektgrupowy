package com.example.pc.flatmaps.Interfaces.MapFragment;

import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

public interface MapFragmentPresenter
{
    public void onStart();
    public void onDestroy();
    public void onMarkerClick(Marker marker);
    public void getAds(List<AdsResponse> adsResponses);
}

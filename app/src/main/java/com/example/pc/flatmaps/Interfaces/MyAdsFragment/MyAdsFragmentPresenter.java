package com.example.pc.flatmaps.Interfaces.MyAdsFragment;

import com.example.pc.flatmaps.Models.Responses.AdsResponse;

public interface MyAdsFragmentPresenter
{
    public void onStart(String userLogin);
    public void onDestroy();
    public void deleteAds(AdsResponse ads);
}

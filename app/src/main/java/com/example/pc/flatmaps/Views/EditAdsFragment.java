package com.example.pc.flatmaps.Views;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.HelpClassess.PermissionHelper;
import com.example.pc.flatmaps.Interfaces.EditAdsFragment.EditAdsFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.EditAdsFragment.EditAdsFragmentView;
import com.example.pc.flatmaps.Models.Requests.EditAdsRequest;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.databinding.FragmentEditAdsBinding;
import com.google.gson.Gson;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

import static com.example.pc.flatmaps.Views.AddAdsFragment.CAMERA_REQUEST;
import static com.example.pc.flatmaps.Views.AddAdsFragment.FILE_PROVIDER;
import static com.example.pc.flatmaps.Views.AddAdsFragment.PHOTO_EXTENSION;
import static com.example.pc.flatmaps.Views.AddAdsFragment.PHOTO_NAME;
import static com.example.pc.flatmaps.Views.MyAdsFragment.ADS;

public class EditAdsFragment extends Fragment implements EditAdsFragmentView
{
    @Inject
    EditAdsFragmentPresenter mPresenter;

    @Inject
    String userLogin;

    @Inject
    Context mContext;

    @BindView(R.id.etEditCity)
    EditText etEditCity;

    @BindView(R.id.etEditStreet)
    EditText etEditStreet;

    @BindView(R.id.etEditNumber)
    EditText etEditNumber;

    @BindView(R.id.etEditPrice)
    EditText etEditPrice;

    @BindView(R.id.spEditNumberOfRooms)
    Spinner spEditNumberOfRooms;

    @BindView(R.id.etEditSize)
    EditText etEditSize;

    @BindView(R.id.srlEditAds)
    SwipeRefreshLayout srlEditAds;

    private Unbinder unbinder;
    private FragmentEditAdsBinding binding;
    private Bundle bundle;
    private File photoFile;
    private Uri uriToFile;

    private AdsResponse getAds()
    {
        bundle = this.getArguments();
        String adsJson = bundle.getString(ADS);
        return new Gson().fromJson(adsJson, AdsResponse.class);
    }

    private String getCity() {
        return etEditCity.getText().toString();
    }

    private String getStreet() {
        return etEditStreet.getText().toString();
    }

    private String getNumber() {
        return etEditNumber.getText().toString();
    }

    private String getPrice() {
        return etEditPrice.getText().toString();
    }

    private String getNumberOfRooms() {
        return spEditNumberOfRooms.getSelectedItem().toString();
    }

    private String getSize() {
        return etEditSize.getText().toString();
    }

    private Date getTodayDate()
    {
        return Calendar.getInstance().getTime();
    }

    @Inject
    public EditAdsFragment() {}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) 
    {
        int title = R.string.edit_ads_fragment_title;
        Manager.changeActionBarTitle(getActivity(), title);

        AndroidSupportInjection.inject(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_edit_ads, parent, false);
        binding.setFragment(this);
        binding.setAds(getAds());

        unbinder = ButterKnife.bind(this, binding.getRoot());

        return binding.getRoot();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        unbinder.unbind();

        binding.unbind();

        mPresenter.onDestroy();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.onStart();
    }

    @Override
    public void onPause() 
    {
        super.onPause();

        mPresenter.onPause();
    }

    @Override
    public void onResume() 
    {
        super.onResume();

        mPresenter.onResume();
    }

    @Override
    public void toggleSwipeLayout(boolean enabled)
    {
        srlEditAds.setRefreshing(enabled);
    }

    @Override
    public void base64Created(String imageBase64) {}

    @Override
    public void onEditSuccessfull()
    {
        showToast("Ads edited");

        Manager.startNewFragment(getFragmentManager(), new MyAdsFragment());
    }

    @Override
    public void showToast(String errorMessage) {
        Manager.showToast(mContext, errorMessage);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        setupSpinner();

        setupSRL();
    }

    private void setupSRL()
    {
        srlEditAds.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        srlEditAds.setEnabled(false);
    }

    private void setupSpinner()
    {
        String[] roomTypesList = Manager.getStringArray(mContext, R.array.roomTypesList);

        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.spinner_row_item, roomTypesList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spEditNumberOfRooms.setAdapter(adapter);
    }

    public void btnEditAddClick(View view)
    {
        int adsId = getAds().getId_ogloszenia();

        EditAdsRequest request = new EditAdsRequest(getCity(), getStreet(), getNumber(), getPrice(), getNumberOfRooms(), getSize(), userLogin, adsId, getTodayDate());

        mPresenter.editAds(photoFile, request);
    }

    public void ivEditCameraClick(View view)
    {
        if (Manager.isCameraPermissionGranted(mContext))
            startCameraIntent();

        else
            new PermissionHelper().checkAndRequestPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
    }

    private void startCameraIntent()
    {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        String photoName = PHOTO_NAME + new Date().getTime() + PHOTO_EXTENSION;
        photoFile = new File(Environment.getExternalStorageDirectory() + File.separator + photoName);
        uriToFile = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, photoFile);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriToFile);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }
}
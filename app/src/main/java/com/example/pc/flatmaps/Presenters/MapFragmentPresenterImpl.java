package com.example.pc.flatmaps.Presenters;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.MapFragment.MapFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.MapFragment.MapFragmentView;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MapFragmentPresenterImpl implements MapFragmentPresenter
{
    private MapFragmentView mFragmentView;
    private UserInterface mUserInterface;
    private Subscriber<List<AdsResponse>> mAdsSubscriber;
    private Observable<List<AdsResponse>> mAdsObservable;
    private List<AdsResponse> adsList;

    @Inject
    public MapFragmentPresenterImpl(MapFragmentView mapFragmentView, UserInterface userInterface)
    {
        mFragmentView = mapFragmentView;
        mUserInterface = userInterface;
        adsList = new ArrayList<>();
    }

    @Override
    public void onStart() {}

    @Override
    public void getAds(List<AdsResponse> adsResponses)
    {
        if(!isItFilter(adsResponses))
            getAds();

        else
            handleFilter(adsResponses);
    }

    private void handleFilter(List<AdsResponse> adsResponses)
    {
        adsList = adsResponses;

        addMarkers();
    }

    private boolean isItFilter(List<AdsResponse> adsResponses)
    {
        return (adsResponses.size() > 0);
    }

    private void getAds()
    {
        createSubsciber();

        createObservable();

        adsSubscribe();
    }

    private void adsSubscribe()
    {
        mAdsObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(mAdsSubscriber);
    }

    private void createObservable()
    {
        mAdsObservable = mUserInterface.getAds();
    }

    private void createSubsciber()
    {
        mAdsSubscriber = new Subscriber<List<AdsResponse>>() {
            @Override
            public void onCompleted() {}

            @Override
            public void onError(Throwable e)
            {
                mFragmentView.showToast(e.getMessage());
            }

            @Override
            public void onNext(List<AdsResponse> adsResponses)
            {
                adsList = adsResponses;

                addMarkers();
            }
        };
    }

    private void addMarkers()
    {
        for(AdsResponse ads: adsList) {
            MarkerOptions markerOptions = getMarkerOption(ads);
            mFragmentView.addMarker(markerOptions);
        }
    }

    private MarkerOptions getMarkerOption(AdsResponse ads)
    {
        float x = ads.getX();
        float y = ads.getY();

        int id = ads.getId_ogloszenia();
        String title = String.valueOf(id);

        LatLng markerPosition = new LatLng(x, y);

        return new MarkerOptions()
                .position(markerPosition)
                .title(title);
    }

    @Override
    public void onDestroy()
    {
        Manager.unSubscribe(mAdsSubscriber);
    }

    @Override
    public void onMarkerClick(Marker marker)
    {
        String markerTitle = marker.getTitle();
        int markerID = Integer.parseInt(markerTitle);

        AdsResponse choosenAds = null;

        for(AdsResponse ads : adsList)
            if(ads.getId_ogloszenia() == markerID)
                choosenAds = ads;

        mFragmentView.onMarkerSelected(choosenAds);
    }
}

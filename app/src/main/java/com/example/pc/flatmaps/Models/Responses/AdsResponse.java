package com.example.pc.flatmaps.Models.Responses;

public class AdsResponse
{
    private float x;
    private float y;
    private int id_ogloszenia;
    private String adres;
    private String ilosc_pokoi;
    private String metraz;
    private int cena;
    private String url;
    private String data;
    private String kontakt;
    private String city;
    private String street;
    private String number;

    public AdsResponse(int x, int y, int id_ogloszenia, String adres, String ilosc_pokoi, String metraz, int cena, String url, String data, String kontakt) {
        this.x = x;
        this.y = y;
        this.id_ogloszenia = id_ogloszenia;
        this.adres = adres;
        this.ilosc_pokoi = ilosc_pokoi;
        this.metraz = metraz;
        this.cena = cena;
        this.url = url;
        this.data = data;
        this.kontakt = kontakt;
    }

    public float getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getId_ogloszenia() {
        return id_ogloszenia;
    }

    public void setId_ogloszenia(int id_ogloszenia) {
        this.id_ogloszenia = id_ogloszenia;
    }

    public String getAdres() {
        return adres;
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getIlosc_pokoi() {
        return ilosc_pokoi;
    }

    public void setIlosc_pokoi(String ilosc_pokoi) {
        this.ilosc_pokoi = ilosc_pokoi;
    }

    public String getMetraz() {
        return metraz;
    }

    public void setMetraz(String metraz) {
        this.metraz = metraz;
    }

    public int getCena() {
        return cena;
    }

    public void setCena(int cena) {
        this.cena = cena;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getKontakt(){
        return kontakt;
    }

    public void setKontakt(String kontakt){
        this.kontakt = kontakt;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}

package com.example.pc.flatmaps.Dagger;

import com.example.pc.flatmaps.MyApp;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Singleton
@Component (modules = {AndroidInjectionModule.class, AppModule.class, MyModule.class })
public interface AppComponent extends AndroidInjector<MyApp> {}

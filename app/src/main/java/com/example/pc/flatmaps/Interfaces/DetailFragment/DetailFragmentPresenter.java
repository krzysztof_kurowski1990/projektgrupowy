package com.example.pc.flatmaps.Interfaces.DetailFragment;

import com.example.pc.flatmaps.Models.Requests.AddAdsRequest;

public interface DetailFragmentPresenter
{
    public void onStart();
    public void onDestroy();
}

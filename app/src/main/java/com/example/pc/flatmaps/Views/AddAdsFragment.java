package com.example.pc.flatmaps.Views;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.HelpClassess.PermissionHelper;
import com.example.pc.flatmaps.Interfaces.AddAdsFragment.AddAdsFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.AddAdsFragment.AddAdsFragmentView;
import com.example.pc.flatmaps.Models.Requests.AddAdsRequest;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.databinding.FragmentAddAdsBinding;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

public class AddAdsFragment extends Fragment implements AddAdsFragmentView
{
    @Inject
    AddAdsFragmentPresenter mPresenter;

    @Inject
    String userLogin;

    @Inject
    Context mContext;

    @BindView(R.id.etSearchCity)
    EditText etCity;

    @BindView(R.id.etSearchStreet)
    EditText etStreet;

    @BindView(R.id.etNumber)
    EditText etNumber;

    @BindView(R.id.etPrice)
    EditText etPrice;

    @BindView(R.id.spSearchNumberOfRooms)
    Spinner spNumberOfRooms;

    @BindView(R.id.etSize)
    EditText etSize;

    @BindView(R.id.srlAddAds)
    SwipeRefreshLayout srlAddAds;

    public static final int CAMERA_REQUEST = 100;
    public static final String DATA = "data";
    public static final String FILE_PROVIDER = "com.example.pc.flatmaps.provider";

    public static final String PHOTO_NAME = "Photo";
    public static final String PHOTO_EXTENSION = ".png";
    public static final String PHOTO_PART_NAME = "file";

    private Unbinder unbinder;
    private FragmentAddAdsBinding binding;
    private File photoFile;
    private Uri uriToFile;

    private String getCity()
    {
        return Manager.getText(etCity);
    }

    private String getStreet()
    {
        return Manager.getText(etStreet);
    }

    private String getNumber()
    {
        return Manager.getText(etNumber);
    }

    private String getPrice()
    {
        return Manager.getText(etPrice);
    }

    private String getNumberOfRooms()
    {
        return spNumberOfRooms.getSelectedItem().toString();
    }

    private String getSize()
    {
        return Manager.getText(etSize);
    }

    private Date getTodayDate()
    {
        return Calendar.getInstance().getTime();
    }

    @Inject
    public AddAdsFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        int title = R.string.add_ads_fragment_title;

        Manager.changeActionBarTitle(getActivity(), title);

        AndroidSupportInjection.inject(this);

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_ads, parent, false);
        binding.setFragment(this);

        unbinder = ButterKnife.bind(this, binding.getRoot());

        return binding.getRoot();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        unbinder.unbind();

        binding.unbind();

        mPresenter.onDestroy();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.onStart();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        mPresenter.onPause();
    }

    @Override
    public void onResume()
    {
        super.onResume();

        mPresenter.onResume();
    }

    @Override
    public void base64Created(String imageBase64) {}

    @Override
    public void toggleSwipeLayout(boolean enabled)
    {
        srlAddAds.setRefreshing(enabled);
    }

    @Override
    public void showToast(String errorMessage)
    {
        Manager.showToast(mContext, errorMessage);
    }

    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        setupSpinner();

        setupSRL();
    }

    private void setupSRL()
    {
        srlAddAds.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);

        srlAddAds.setEnabled(false);
    }

    private void setupSpinner()
    {
        String[] roomTypesList = Manager.getStringArray(mContext, R.array.roomTypesList);

        ArrayAdapter adapter = new ArrayAdapter(getActivity(), R.layout.spinner_row_item, roomTypesList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spNumberOfRooms.setAdapter(adapter);
    }

    public void btnAddClick(View view)
    {
        AddAdsRequest request = new AddAdsRequest(getCity(), getStreet(), getNumber(), getPrice(), getNumberOfRooms(), getSize(), userLogin, getTodayDate());
        mPresenter.addNewAds(photoFile, request);
    }

    public void ivCameraClick(View view)
    {
        if(Manager.isCameraPermissionGranted(mContext))
            startCameraIntent();

        else
            new PermissionHelper().checkAndRequestPermissions(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
    }

    private void startCameraIntent()
    {
        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        String photoName = PHOTO_NAME + new Date().getTime() + PHOTO_EXTENSION;
        photoFile = new File(Environment.getExternalStorageDirectory() + File.separator + photoName);
        uriToFile = FileProvider.getUriForFile(getActivity(), FILE_PROVIDER, photoFile);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriToFile);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }
}

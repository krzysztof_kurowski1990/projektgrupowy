package com.example.pc.flatmaps.Presenters;

import android.graphics.Bitmap;

import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.AddAdsFragment.AddAdsFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.AddAdsFragment.AddAdsFragmentView;
import com.example.pc.flatmaps.Models.Requests.AddAdsRequest;
import com.example.pc.flatmaps.Retrofit.Interfaces.UserInterface;
import com.example.pc.flatmaps.Validator.InputValidator;
import com.google.gson.Gson;

import java.io.File;

import javax.inject.Inject;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static com.example.pc.flatmaps.Views.AddAdsFragment.PHOTO_PART_NAME;

public class AddAdsFragmentPresenterImpl implements AddAdsFragmentPresenter
{
    public static final String MEDIA_TYPE_MULTIPART = "multipart/form-data";
    public static final String MEDIA_TYPE_TEXT_PLAIN = "text/plain";

    private AddAdsFragmentView mFragmentView;
    private UserInterface mUserInterface;
    private Subscriber<Response<Void>> mAddAdsSubscriber;
    private Observable<Response<Void>> mAddAdsObservable;

    @Inject
    public AddAdsFragmentPresenterImpl(AddAdsFragmentView addAdsFragmentView, UserInterface userInterface)
    {
        mFragmentView = addAdsFragmentView;
        mUserInterface = userInterface;
    }

    @Override
    public void onStart() {}

    @Override
    public void onPause(){}


    @Override
    public void onResume(){}

    @Override
    public void onDestroy()
    {
        Manager.unSubscribe(mAddAdsSubscriber);
    }

    @Override
    public void createBase64FromBitmap(Bitmap bitmap)
    {
        String imageBase64 = Manager.convertBitmapToBase64(bitmap);

        mFragmentView.base64Created(imageBase64);
    }

    @Override
    public void addNewAds(File imageFile, AddAdsRequest request)
    {
        if(!isImageCreated(imageFile))
        {
            mFragmentView.showToast("Please make photo first");
            return;
        }

        if(!isRequestValidate(request))
        {
            mFragmentView.showToast(InputValidator.errorMessage);
            return;
        }

        createObservable(getImageBody(imageFile), request);

        createSubsciber();

        addAddsSubscribe();

    }

    private boolean isRequestValidate(AddAdsRequest request)
    {
        return (InputValidator.isInputCorrect(
                request.getCity(),
                request.getLogin(),
                request.getNumber(),
                request.getNumberOfRooms(),
                request.getPrice(),
                request.getSize(),
                request.getStreet(),
                request.getDate()+""));
    }

    private boolean isImageCreated(File imageFile)
    {
        return (imageFile != null);
    }

    private RequestBody getJsonBody(AddAdsRequest request)
    {
        String json = new Gson().toJson(request);
        return RequestBody.create(MediaType.parse(MEDIA_TYPE_TEXT_PLAIN), json);
    }

    private MultipartBody.Part getImageBody(File imageFile)
    {
        RequestBody requestBody = RequestBody.create(MediaType.parse(MEDIA_TYPE_MULTIPART), imageFile);
        return MultipartBody.Part.createFormData(PHOTO_PART_NAME, imageFile.getName(), requestBody);
    }

    private void createSubsciber()
    {
        mAddAdsSubscriber = new Subscriber<Response<Void>>()
        {
            @Override
            public void onCompleted()
            {
                mFragmentView.toggleSwipeLayout(false);
            }

            @Override
            public void onError(Throwable e)
            {
                mFragmentView.showToast(e.getMessage());

                mFragmentView.toggleSwipeLayout(false);
            }

            @Override
            public void onNext(Response<Void> response) {
                if(Manager.isResponseSuccessfull(response))
                    mFragmentView.showToast("Ads added");
            }
        };
    }

    private void createObservable(MultipartBody.Part multipartBody, AddAdsRequest request)
    {
        mAddAdsObservable = mUserInterface.addAds(multipartBody, request);
    }

    private void addAddsSubscribe()
    {

        mFragmentView.toggleSwipeLayout(true);

        mAddAdsObservable
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .retry(1)
                .subscribe(mAddAdsSubscriber);
    }
}

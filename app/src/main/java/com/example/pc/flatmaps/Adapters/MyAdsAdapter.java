package com.example.pc.flatmaps.Adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.pc.flatmaps.Interfaces.MyAdsAdapter.MyAdsClickListener;
import com.example.pc.flatmaps.Models.Responses.AdsResponse;
import com.example.pc.flatmaps.databinding.RowMyAdsAdapterBinding;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MyAdsAdapter extends RecyclerView.Adapter<MyAdsAdapter.ViewHolder>
{
    private List<AdsResponse> mAds;
    private Context mContext;
    private MyAdsClickListener mListener;
    private int mRowLayout;
    private Geocoder geocoder;

    public MyAdsAdapter(int rowLayout, Context context, MyAdsClickListener listener)
    {
        mRowLayout = rowLayout;
        mContext = context;
        mAds = new ArrayList<>();
        mListener = listener;
        geocoder = new Geocoder(mContext, Locale.getDefault());
    }

    public void addAds(AdsResponse newItem)
    {
        mAds.add(newItem);
        notifyItemInserted(getItemCount());
    }

    public void removeAds(AdsResponse ads)
    {
        int position = getPosition(ads);
        mAds.remove(ads);
        notifyItemRemoved(position);
    }

    public int getPosition(AdsResponse ads)
    {
        return mAds.indexOf(ads);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i)
    {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        RowMyAdsAdapterBinding binding = DataBindingUtil.inflate(inflater, mRowLayout, viewGroup, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        AdsResponse ads = mAds.get(position);

        holder.bind(ads);

        setOnLongClick(holder);
    }

    private void setOnLongClick(ViewHolder holder)
    {
        holder
                .mBinding
                .getRoot()
                .setOnLongClickListener(view -> {
                    showDialog(holder.mBinding.getAds());
                    return false;
                });
    }

    private void showDialog(AdsResponse ads)
    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

        setMessage(builder);

        setPositiveButton(builder, ads);

        setNegativeButton(builder, ads);

        setCancelButton(builder);

        builder.show();
    }

    private void setCancelButton(AlertDialog.Builder builder)
    {
        builder
                .setNeutralButton(
                    "Cancel",
                    (dialog, which) ->
                        dialog.dismiss());
    }

    private void setNegativeButton(AlertDialog.Builder builder, AdsResponse ads)
    {
        builder
                .setNegativeButton(
                        "Edit",
                        (dialog, which) ->
                        {
                                addDataToAddress(ads);
                                mListener.edit(ads);
                        });
    }

    private void addDataToAddress(AdsResponse ads)
    {
        List<Address> addresses = null;
        try
        {
            addresses = geocoder.getFromLocation(ads.getX(), ads.getY(), 1);
            ads.setCity(getCity(addresses));
            ads.setNumber(getNumber(addresses));
            ads.setStreet(getStreet(addresses));
        }

        catch (IOException e)
        {
            mListener.showToast("Cant get location from coordinations");

            e.printStackTrace();
        }
    }

    private String getStreet(List<Address> addresses)
    {
        return (addresses.get(0).getThoroughfare());
    }

    private String getNumber(List<Address> addresses)
    {
        return (addresses.get(0).getSubThoroughfare());
    }

    private String getCity(List<Address> addresses)
    {
        return (addresses.get(0).getLocality());
    }

    private void setPositiveButton(AlertDialog.Builder builder, AdsResponse ads)
    {
        builder
                .setPositiveButton(
                        "Delete",
                        (dialog, which) ->
                                mListener.delete(ads));
    }

    private void setMessage(AlertDialog.Builder builder)
    {
        builder.setMessage("Co chcesz zrobic?");
    }

    @Override
    public int getItemCount() {
        return mAds.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {
        private RowMyAdsAdapterBinding mBinding;

        public ViewHolder(RowMyAdsAdapterBinding binding)
        {
            super(binding.getRoot());
            mBinding = binding;
        }

        public void bind(AdsResponse ads)
        {
            mBinding.setAds(ads);
            mBinding.executePendingBindings();
        }
    }
}

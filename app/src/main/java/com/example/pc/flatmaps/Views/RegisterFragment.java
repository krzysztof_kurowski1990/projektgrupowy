package com.example.pc.flatmaps.Views;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.example.pc.flatmaps.HelpClassess.Keys;
import com.example.pc.flatmaps.HelpClassess.Manager;
import com.example.pc.flatmaps.Interfaces.RegisterFragment.RegisterFragmentPresenter;
import com.example.pc.flatmaps.Interfaces.RegisterFragment.RegisterFragmentView;
import com.example.pc.flatmaps.Models.Requests.RegisterRequest;
import com.example.pc.flatmaps.R;
import com.example.pc.flatmaps.databinding.FragmentRegisterBinding;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.AndroidSupportInjection;

public class RegisterFragment extends Fragment implements RegisterFragmentView
{
    @Inject
    RegisterFragmentPresenter mPresenter;

    @Inject
    Context mContext;

    @BindView(R.id.etRegisterLogin)
    EditText etRegisterLogin;

    @BindView(R.id.etRegisterPassword)
    EditText etRegisterPassword;

    @BindView(R.id.etRegisterPassword2)
    EditText etRegisterPassword2;

    private FragmentRegisterBinding binder;
    private Unbinder unbinder;

    private String getPassword()
    {
        return Manager.getText(etRegisterPassword);
    }

    private String getPassword2()
    {
        return Manager.getText(etRegisterPassword2);
    }

    private String getLogin()
    {
        return Manager.getText(etRegisterLogin);
    }

    @Inject
    public RegisterFragment(){}

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        AndroidSupportInjection.inject(this);

        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_register, parent, false);
        binder.setFragment(this);

        unbinder = ButterKnife.bind(this, binder.getRoot());

        return binder.getRoot();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();

        unbinder.unbind();

        binder.unbind();

        mPresenter.onDestroy();
    }

    @Override
    public void onStart()
    {
        super.onStart();

        mPresenter.onStart();
    }

    @Override
    public void showToast(String message)
    {
        Manager.showToast(mContext, message);
    }

    @Override
    public void successSaveAccount()
    {
        showToast("Register successfull");

        Manager.saveInSp(mContext, Keys.LOGIN, getLogin());

        getFragmentManager().popBackStack();
    }

    public void btnRegisterClick(View view)
    {
        RegisterRequest registerRequest = new RegisterRequest(getLogin(), getPassword(), getPassword2());
        mPresenter.saveAccount(registerRequest);
    }

    public void onViewCreated(View view, Bundle savedInstanceState){}
}
